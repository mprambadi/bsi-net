using System.ComponentModel.DataAnnotations;

namespace HelloApi.Infrastructure.Data.Models
{
    public class Todo
    {
        public int Id { get; set; }

        public string? Title { get; set; }

        public string? Secret { get; set; }

        public bool Status { get; set; }

        public int UserId { get; set; }

        public virtual User? User { get; set; }
    }

}
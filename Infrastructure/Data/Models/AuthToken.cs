﻿using System;
namespace HelloApi.Infrastructure.Data.Models
{
    public class AuthToken
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string? Token { get; set; }
        public string? Role { get; set; }
        public int ExpiredAt { get; set; }
    }
}


using Microsoft.Extensions.Options;

namespace HelloApi.Infrastructure.Services;

public class WaService : IOtpService
{
    private readonly WaSettings _waSettings;
    public WaService(IOptions<WaSettings> waSettings)
    {
        _waSettings = waSettings.Value;
    }
    public async Task Send(MailRequest mailRequest)
    {
        var client = new HttpClient();
        var request = new HttpRequestMessage
        {
            Method = HttpMethod.Post,
            RequestUri = new Uri("https://graph.facebook.com/v14.0/102369652499206/messages"),
            Headers =
    {
        { "Authorization", "Bearer "+_waSettings.Token },
    },
            Content = new StringContent("{\"messaging_product\": \"whatsapp\", \"recipient_type\": \"individual\", \"to\": \"6283863385061\", \"type\": \"text\", \"text\": {   \"preview_url\": false,   \"body\": " + mailRequest.Body + " }}")
            {
                Headers =
        {
            ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json")
        }
            }
        };
        using (var response = await client.SendAsync(request))
        {
            response.EnsureSuccessStatusCode();
            var body = await response.Content.ReadAsStringAsync();
            Console.WriteLine(body);
        }
    }
}


using HelloApi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;


namespace HelloApi.Infrastructure;


public class Depedencies
{


    public static void ConfigureService(IConfiguration configuration, IServiceCollection services)
    {
        services.AddDbContext<AppDbContext>(options => options.UseLazyLoadingProxies().UseSqlServer(configuration["DB"]));
    }
}


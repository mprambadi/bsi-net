﻿
using System;
namespace HelloApi.Infrastructure.Dto
{
    public class LoginRequest
    {
       public string? Username { get; set; }

       public string? Password { get; set; }
    }
}


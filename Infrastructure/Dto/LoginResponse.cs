﻿using System;
namespace HelloApi.Infrastructure.Dto
{
    public class LoginResponse
    {
        public string? Name {get; set;}
        public string? Username {get; set;}
        public string? Token {get; set;}
        public int ExpiredAt { get; set; }
    }
}


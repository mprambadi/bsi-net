## Setup

```bash
cp secret.example.json to secret.json

dotnet user-secrets init 


# Mac /Linux
cat ./secret.json | dotnet user-secrets set 

# Windows 
type ./secret.json | dotnet user-secrets set


dotnet user-secrets list

```


Create migration 
```
dotnet ef migrations add InitialCreate -o Infrastructure/Data/Migrations
```
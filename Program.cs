using Microsoft.EntityFrameworkCore;

using System.Text;
using HelloApi.Infrastructure.Data.Models;
using HelloApi.Infrastructure.Services;
using HelloApi.Infrastructure.Data;
using HelloApi.Infrastructure;
using HelloApi.Infrastructure.Shared;
using HelloApi.Infrastructure.Dto;

using Microsoft.OpenApi.Models;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text.Json;
using Microsoft.Extensions.Caching.Distributed;

var builder = WebApplication.CreateBuilder(args);

var securityScheme = new OpenApiSecurityScheme()
{
    Name = "Authorization",
    Type = SecuritySchemeType.ApiKey,
    Scheme = "Bearer",
    BearerFormat = "JWT",
    In = ParameterLocation.Header,
    Description = "JSON Web Token based security",
};

var securityReq = new OpenApiSecurityRequirement()
{
    {
        new OpenApiSecurityScheme
        {
            Reference = new OpenApiReference
            {
                Type = ReferenceType.SecurityScheme,
                Id = "Bearer"
            }
        },
        new string[] {}
    }

};



builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = builder.Configuration["Redis"];
    options.InstanceName = "RedisDemo_";
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("Bearer", securityScheme);
    options.AddSecurityRequirement(securityReq);
});


builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"])),
        ValidateIssuer = false,
        ValidateIssuerSigningKey = true,
        ValidateLifetime = false,
        ValidateAudience = false,
        RequireExpirationTime = true
    };
});

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

Depedencies.ConfigureService(builder.Configuration, builder.Services);
builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.Configure<MailSettings>(builder.Configuration.GetSection("Mail"));
builder.Services.Configure<WaSettings>(builder.Configuration.GetSection("Wa"));
builder.Services.AddSingleton<IOtpService, MailService>();
builder.Services.AddSingleton<IOtpService, WaService>();

builder.Services.Configure<Microsoft.AspNetCore.Http.Json.JsonOptions>(options =>
{
    options.SerializerOptions.ReferenceHandler = System.Text.Json.Serialization.ReferenceHandler.IgnoreCycles;
});


builder.Services.AddAuthorization();


var app = builder.Build();

// Configure the HTTP request pipeline.

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


var summaries = new[]
{
    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
};

app.MapGet("/hello", () =>
{
    var forecast = Enumerable.Range(1, 5).Select(index =>
        new WeatherForecast
        (
            DateTime.Now.AddDays(index),
            Random.Shared.Next(-20, 55),
            summaries[Random.Shared.Next(summaries.Length)]
        ))
        .ToArray();

    return forecast;
})
.WithName("GetWeatherForecast");

app.MapGet("/todos", [Authorize]
async (AppDbContext db, HttpContext context) =>
{
    var tokenData = new Jwt().GetTokenClaim(context);

    return await db.Todos.Include(todo => todo.User).Select(todo => new TodoDTO(todo))
    .ToListAsync();
});

app.MapPost("/todos", [Authorize] async (Todo todo, AppDbContext db, HttpContext context) =>
{

    var tokenData = new Jwt().GetTokenClaim(context);

    todo.UserId = int.Parse(tokenData.Id);
    db.Todos.Add(todo);
    await db.SaveChangesAsync();
    return Results.Created($"/todos/{todo.Id}", todo);
});

app.MapGet("/todos/{id}", async (int id, AppDbContext db) =>
    await db.Todos.FindAsync(id)
        is Todo todo
            ? Results.Ok(todo)
            : Results.NotFound()
);

app.MapPut("/todos/{id}", async (int id, Todo editTodo, AppDbContext db) =>
{
    var todo = await db.Todos.FindAsync(id);

    if (todo is null) return Results.NotFound();

    todo.Title = editTodo.Title;

    await db.SaveChangesAsync();

    return Results.NoContent();

});

app.MapDelete("/todos/{id}", async (int id, AppDbContext db) =>
{
    var todo = await db.Todos.FindAsync(id);

    if (todo is null) return Results.NotFound();



    db.Todos.Remove(todo);
    await db.SaveChangesAsync();

    return Results.NoContent();

});

app.MapGet("/users", [Authorize] async Task<object> (IDistributedCache cache, AppDbContext db) =>
{
    var response = await cache.GetUsers(db);

    return response;
});

app.MapPost("/activate", async (MailRequest mailRequest, IOtpService mail, IOtpService wa) =>
{


    await mail.Send(mailRequest);
    await wa.Send(mailRequest);


    return Results.Ok();
});

app.MapPost("/register", async (User request, AppDbContext db) =>
{
    var result = await db.Users.Where(item => item.Username == request.Username).FirstOrDefaultAsync();

    if (result != null)
    {
        return Results.BadRequest(new { message = "Username already exist" });
    }

    var passwordHash = BCrypt.Net.BCrypt.HashPassword(request.Password);
    request.Password = passwordHash;

    db.Users.Add(request);
    await db.SaveChangesAsync();

    return Results.Ok(new { message = "Registration succesfully" });

});

app.MapPost("/login", async (LoginRequest request, AppDbContext db) =>
{
    // verify username user
    var result = await db.Users.Where(item => item.Username == request.Username).FirstOrDefaultAsync();

    if (result == null)
    {
        return Results.BadRequest(new { message = "Username incorrect" });
    }

    bool verifyPassword = BCrypt.Net.BCrypt.Verify(request.Password, result.Password);

    // verify password
    if (!verifyPassword)
    {
        return Results.BadRequest(new { message = "Password Incorrect" });
    }

    var token = new Jwt().GenerateJwtToken(result);
    var expiredAt = (Int32)DateTime.UtcNow.AddDays(3).Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

    // insert token to auth token
    var authToken = new AuthToken
    {
        UserId = result.Id,
        Role = "admin",
        ExpiredAt = expiredAt,
        Token = token,
    };

    db.AuthTokens.Add(authToken);
    await db.SaveChangesAsync();


    return Results.Ok(new LoginResponse
    {
        Name = result.Name,
        Username = result.Username,
        ExpiredAt = expiredAt,
        Token = token,
    });
});

app.MapPost("/upload", async (HttpRequest request, AppDbContext db) =>
{


    foreach (var formFile in request.Form.Files)
    {
        if (formFile.Length > 0)
        {

            Random r = new Random();
            int n = r.Next();
            TimeSpan t = DateTime.Now - new DateTime(1970, 1, 1);
            int secondsSinceEpoch = (int)t.TotalSeconds;

            var extension = new FileInfo(formFile.FileName);
            var fileName = $"{secondsSinceEpoch}{n}{extension.Extension}";
            var filePath = Path.Combine("image", fileName);

            using (var stream = System.IO.File.Create(filePath))
            {
                await formFile.CopyToAsync(stream);
            }
        }
    }

    return Results.Ok();
}).Accepts<IFormFile>("*");

app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();

app.Run();

record WeatherForecast(DateTime Date, int TemperatureC, string? Summary)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}


class UserItem
{
    public UserItem(User user) => (Name, Username) = (user.Name, user.Username);
    public string? Name { get; set; }

    public string? Username { get; set; }
}
class TodoDTO
{
    public int Id { get; set; }

    public string? Title { get; set; }

    public UserItem User { get; set; }
    public TodoDTO(Todo todoItem) =>
    (Id, Title, User) = (todoItem.Id, todoItem.Title, new UserItem(todoItem.User));

}


class TodoItem
{
    public string? Title { get; set; }

    public TodoItem(Todo todo) => (Title) = (todo.Title);
}

class UserDTO
{
    public int Id { get; set; }
    public string? Name { get; set; }

    // public List<TodoItem> Todos { get; set; }

    // public UserDTO(User user) =>
    // (Id, Name, Todos) = (user.Id, user.Name, user.Todos.Select(todo => new TodoItem(todo)).ToList());
}

public class MailSettings
{
    public string? Host { get; set; }
    public int Port { get; set; }
    public string? Password { get; set; }
    public string? Username { get; set; }
    public string? From { get; set; }
}

public class WaSettings
{
    public string? Token { get; set; }
}

public interface IOtpService
{
    Task Send(MailRequest mailRequest);
}


public class MailRequest
{
    public string? ToEmail { get; set; }
    public string? Subject { get; set; }
    public string? Body { get; set; }
}

public class Token
{
    public string? Id { get; set; }
    public string? Name { get; set; }
}


internal static class DistribuitedCacheExtentions
{
    public static async Task SetRecordAsync<T>(
            this IDistributedCache cache,
            string recordId,
            T data,
            TimeSpan? absoluteExpireTime = null)
    {
        var options = new DistributedCacheEntryOptions();

        options.AbsoluteExpirationRelativeToNow = absoluteExpireTime ?? TimeSpan.FromSeconds(60);

        var jsonData = JsonSerializer.Serialize(data);

        await cache.SetStringAsync(recordId, jsonData, options);
    }

    public static async Task<T> GetRecordAsync<T>(
            this IDistributedCache cache,
            string recordId)
    {
        var jsonData = await cache.GetStringAsync(recordId);

        if (jsonData is null) return default(T);

        var dataResponse = JsonSerializer.Deserialize<T>(jsonData);

        return dataResponse;
    }
}

internal static class LoadUsers
{
    public static async Task<object> GetUsers(
        this IDistributedCache cache, AppDbContext db)
    {
        List<User>? users = null; //Declaring empty forecasts

        string recordKey = $"users_{DateTime.Now.ToString("yyyyMMdd_hhmm")}"; //Declaring a unit recordKey to set our get the data

        users = await cache.GetRecordAsync<List<User>>(recordKey);

        if (users != null)
        {
            return Results.Ok(users);
        }

        var result = await db.Users.ToListAsync();


        await cache.SetRecordAsync(recordKey, result);

        return Results.Ok(result);


    }
}